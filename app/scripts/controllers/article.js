'use strict';

/**
 * @ngdoc function
 * @name angularProjectApp.controller:ArticleCtrl
 * @description
 * # ArticleCtrl
 * Controller of the angularProjectApp
 */
angular.module('angularProjectApp')
  .controller('ArticleCtrl', ['$scope','$routeParams', function($scope, $routeParams, $http) {

      var artId = $routeParams.artId;
      $scope.artId = $routeParams.artId;

      $http.get("http://www.w3schools.com/angular/customers.php")
      .success(function(response) {
        $scope.names = response.records;
      });
}]);
